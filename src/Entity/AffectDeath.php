<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: 'App\Repository\AffectDeathRepository')]
#[UniqueEntity('name')]
#[Table]
#[UniqueConstraint(name: 'affect_death_name_unique', columns: ['name'])]
class AffectDeath
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\Column(type: 'string', length: 64)]
    private $name;
    #[ORM\ManyToOne(targetEntity: 'App\Entity\CauseOfDeath')]
    #[ORM\JoinColumn(nullable: false)]
    private $cause;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function getCause(): ?CauseOfDeath
    {
        return $this->cause;
    }
    public function setCause(?CauseOfDeath $cause): self
    {
        $this->cause = $cause;

        return $this;
    }
}
