<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: 'App\Repository\RequireHomeRepository')]
#[UniqueEntity('name')]
#[Table]
#[UniqueConstraint(name: 'require_home_name_unique', columns: ['name'])]
class RequireHome
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\Column(type: 'string', length: 64)]
    private $name;
    #[ORM\Column(type: 'integer', nullable: true)]
    private $minLevel;
    #[ORM\Column(type: 'integer', nullable: true)]
    private $maxLevel;
    #[ORM\ManyToOne(targetEntity: 'App\Entity\CitizenHomeUpgradePrototype')]
    private $upgrade;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function getMinLevel(): ?int
    {
        return $this->minLevel;
    }
    public function setMinLevel(?int $minLevel): self
    {
        $this->minLevel = $minLevel;

        return $this;
    }
    public function getMaxLevel(): ?int
    {
        return $this->maxLevel;
    }
    public function setMaxLevel(?int $maxLevel): self
    {
        $this->maxLevel = $maxLevel;

        return $this;
    }
    public function getUpgrade(): ?CitizenHomeUpgradePrototype
    {
        return $this->upgrade;
    }
    public function setUpgrade(?CitizenHomeUpgradePrototype $upgrade): self
    {
        $this->upgrade = $upgrade;

        return $this;
    }
}
