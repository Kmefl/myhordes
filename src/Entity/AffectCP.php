<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: 'App\Repository\AffectCPRepository')]
#[UniqueEntity('name')]
#[Table]
#[UniqueConstraint(name: 'affect_cp_name_unique', columns: ['name'])]
class AffectCP
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\Column(type: 'string', length: 64)]
    private $name;
    #[ORM\Column(type: 'boolean')]
    private $max;
    #[ORM\Column(type: 'smallint')]
    private $cp;
    #[ORM\Column(type: 'integer')]
    private $bonus = 0;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function getMax(): ?bool
    {
        return $this->max;
    }
    public function setMax(bool $max): self
    {
        $this->max = $max;

        return $this;
    }
    public function getCp(): ?int
    {
        return $this->cp;
    }
    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }
    public function getBonus(): ?int
    {
        return $this->bonus;
    }
    public function setBonus(int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }
}
